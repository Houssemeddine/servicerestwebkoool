<?php

namespace mobileBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * TSubscribe
 *
 * @ORM\Table(name="t_subscribe")
 * @ORM\Entity(repositoryClass="mobileBundle\Repository\TSubscribeRepository")
 */
class TSubscribe
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var \mobileBundle\Entity\TPastry
     *
     * @ORM\ManyToOne(targetEntity="mobileBundle\Entity\TPastry")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="idPastry", referencedColumnName="id")
     * })
     */
    private $idpastry;
    /**
     * @var \mobileBundle\Entity\TUser
     *
     * @ORM\ManyToOne(targetEntity="mobileBundle\Entity\TUser")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="idUser", referencedColumnName="id")
     * })
     */
    private $iduser;
    /**
     * @var \mobileBundle\Entity\TProduit
     *
     * @ORM\ManyToOne(targetEntity="mobileBundle\Entity\TProduit")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="idProduit", referencedColumnName="id")
     * })
     */
    private $idproduit;




    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return TPastry
     */
    public function getIdpastry()
    {
        return $this->idpastry;
    }

    /**
     * @param TPastry $idpastry
     */
    public function setIdpastry($idpastry)
    {
        $this->idpastry = $idpastry;
    }

    /**
     * @return TUser
     */
    public function getIduser()
    {
        return $this->iduser;
    }

    /**
     * @param TUser $iduser
     */
    public function setIduser($iduser)
    {
        $this->iduser = $iduser;
    }

    /**
     * @return TProduit
     */
    public function getIdproduit()
    {
        return $this->idproduit;
    }

    /**
     * @param TProduit $idproduit
     */
    public function setIdproduit($idproduit)
    {
        $this->idproduit = $idproduit;
    }


}

