<?php

namespace mobileBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * TPastry
 * @ORM\Table(name="t_pastry", indexes={@ORM\Index(name="FK_prf5htvmedjbtotawoccfs4lc", columns={"idDesc"})})
 * @ORM\Entity(repositoryClass="mobileBundle\Repository\TPastryRepository")
 */
class TPastry
{
    /**
     * @var float
     *
     * @ORM\Column(name="jpsLongitude", type="float", length=255, nullable=true)
     */
    private $jpsLongitude;
    /**
     * @var float
     *
     * @ORM\Column(name="jpsLatitude", type="float", length=255, nullable=true)
     */
    private $jpsLatitude;

    /**
     * @var string
     *
     * @ORM\Column(name="adresse", type="string", length=255, nullable=true)
     */
    private $adresse;
    /**
     * @var string
     *
     * @ORM\Column(name="siret", type="string", length=255, nullable=true)
     */
    private $siret;

    /**
     * @var string
     *
     * @ORM\Column(name="nom", type="string", length=255, nullable=true)
     */
    private $nom;

    /**
     * @var integer
     *
     * @ORM\Column(name="tel", type="integer", nullable=true)
     */
    private $tel;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \mobileBundle\Entity\TDescription
     *
     * @ORM\ManyToOne(targetEntity="mobileBundle\Entity\TDescription")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="idDesc", referencedColumnName="id")
     * })
     */
    private $iddesc;
    /**
     * @var string
     *
     * @ORM\Column(name="image", type="string", length=255, nullable=true)
     */
    private $image;


    /**
     * @return float
     */
    public function getJpsLongitude()
    {
        return $this->jpsLongitude;
    }

    /**
     * @param float $jpsLongitude
     */
    public function setJpsLongitude($jpsLongitude)
    {
        $this->jpsLongitude = $jpsLongitude;
    }

    /**
     * @return float
     */
    public function getJpsLatitude()
    {
        return $this->jpsLatitude;
    }

    /**
     * @param float $jpsLatitude
     */
    public function setJpsLatitude($jpsLatitude)
    {
        $this->jpsLatitude = $jpsLatitude;
    }



    /**
     * @return string
     */
    public function getAdresse()
    {
        return $this->adresse;
    }

    /**
     * @param string $adresse
     */
    public function setAdresse($adresse)
    {
        $this->adresse = $adresse;
    }

    /**
     * @return string
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * @param string $nom
     */
    public function setNom($nom)
    {
        $this->nom = $nom;
    }

    /**
     * @return int
     */
    public function getTel()
    {
        return $this->tel;
    }

    /**
     * @param int $tel
     */
    public function setTel($tel)
    {
        $this->tel = $tel;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return TDescription
     */
    public function getIddesc()
    {
        return $this->iddesc;
    }

    /**
     * @param TDescription $iddesc
     */
    public function setIddesc($iddesc)
    {
        $this->iddesc = $iddesc;
    }

    /**
     * @return string
     */
    public function getSiret()
    {
        return $this->siret;
    }

    /**
     * @param string $siret
     */
    public function setSiret($siret)
    {
        $this->siret = $siret;
    }

    /**
     * @return string
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * @param string $image
     */
    public function setImage($image)
    {
        $this->image = $image;
    }






}

