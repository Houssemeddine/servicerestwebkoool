<?php

namespace mobileBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * TDescription
 *
 * @ORM\Table(name="t_description")
 * @ORM\Entity
 */
class TDescription
{
    /**
     * @var string
     *
     * @ORM\Column(name="bigDesc", type="string", length=255, nullable=true)
     */
    private $bigdesc;

    /**
     * @var string
     *
     * @ORM\Column(name="smallDesc", type="string", length=255, nullable=true)
     */
    private $smalldesc;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * TDescription constructor.

     */
    public function __construct()
    {

    }


    /**
     * @return string
     */
    public function getBigdesc()
    {
        return $this->bigdesc;
    }


    /**
     * @param string $bigdesc
     */
    public function setBigdesc($bigdesc)
    {
        $this->bigdesc = $bigdesc;
    }

    /**
     * @return string
     */
    public function getSmalldesc()
    {
        return $this->smalldesc;
    }

    /**
     * @param string $smalldesc
     */
    public function setSmalldesc($smalldesc)
    {
        $this->smalldesc = $smalldesc;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }


}

