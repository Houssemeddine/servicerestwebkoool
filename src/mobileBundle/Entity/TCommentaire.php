<?php

namespace mobileBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * TCommentaire
 *
 * @ORM\Table(name="t_commentaire", indexes={@ORM\Index(name="FK_4mklvn9duq0kb320roor75odj", columns={"idPastry"}), @ORM\Index(name="FK_97rhdvrpac9lfrvts92i3mj03", columns={"idUser"})})
 * @ORM\Entity
 */
class TCommentaire
{
    /**
     * @var string
     *
     * @ORM\Column(name="Comentaire", type="string", length=255, nullable=true)
     */
    private $comentaire;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date", type="date", nullable=true)
     */
    private $date;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \mobileBundle\Entity\TPastry
     *
     * @ORM\ManyToOne(targetEntity="mobileBundle\Entity\TPastry")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="idPastry", referencedColumnName="id")
     * })
     */
    private $idpastry;

    /**
     * @var \mobileBundle\Entity\TUser
     *
     * @ORM\ManyToOne(targetEntity="mobileBundle\Entity\TUser")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="idUser", referencedColumnName="id")
     * })
     */
    private $iduser;

    /**
     * @return string
     */
    public function getComentaire()
    {
        return $this->comentaire;
    }

    /**
     * @param string $comentaire
     */
    public function setComentaire($comentaire)
    {
        $this->comentaire = $comentaire;
    }

    /**
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * @param \DateTime $date
     */
    public function setDate($date)
    {
        $this->date = $date;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return TPastry
     */
    public function getIdpastry()
    {
        return $this->idpastry;
    }

    /**
     * @param TPastry $idpastry
     */
    public function setIdpastry($idpastry)
    {
        $this->idpastry = $idpastry;
    }

    /**
     * @return TUser
     */
    public function getIduser()
    {
        return $this->iduser;
    }

    /**
     * @param TUser $iduser
     */
    public function setIduser($iduser)
    {
        $this->iduser = $iduser;
    }


}

