<?php

namespace mobileBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * TProduit
 *
 * @ORM\Table(name="t_produit", indexes={@ORM\Index(name="FK_axtshwfkygvh0yvoejsi9wfpc", columns={"idDesc"}), @ORM\Index(name="FK_j5jq6kg8phv79qt61jr77lny9", columns={"idPastry"})})
 * @ORM\Entity(repositoryClass="mobileBundle\Repository\TProduitRepository")
 */
class TProduit
{
    /**
     * @var string
     *
     * @ORM\Column(name="nom", type="string", length=255, nullable=true)
     */
    private $nom;
    /**
     * @var string
     *
     * @ORM\Column(name="type", type="string", length=255, nullable=true)
     */
    private $type;
    /**
     * @var float
     *
     * @ORM\Column(name="prix", type="float", precision=10, scale=0, nullable=true)
     */
    private $prix;
    /**
     * @var string
     *
     * @ORM\Column(name="image", type="string", length=255, nullable=true)
     */
    private $image;


    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \mobileBundle\Entity\TDescription
     *
     * @ORM\ManyToOne(targetEntity="mobileBundle\Entity\TDescription")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="idDesc", referencedColumnName="id")
     * })
     */
    private $iddesc;

    /**
     * @var \mobileBundle\Entity\TPastry
     *
     * @ORM\ManyToOne(targetEntity="mobileBundle\Entity\TPastry")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="idPastry", referencedColumnName="id")
     * })
     */
    private $idpastry;

    /**
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param string $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    /**
     * @return string
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * @param string $nom
     */
    public function setNom($nom)
    {
        $this->nom = $nom;
    }

    /**
     * @return float
     */
    public function getPrix()
    {
        return $this->prix;
    }

    /**
     * @param float $prix
     */
    public function setPrix($prix)
    {
        $this->prix = $prix;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return TDescription
     */
    public function getIddesc()
    {
        return $this->iddesc;
    }

    /**
     * @param TDescription $iddesc
     */
    public function setIddesc($iddesc)
    {
        $this->iddesc = $iddesc;
    }

    /**
     * @return TPastry
     */
    public function getIdpastry()
    {
        return $this->idpastry;
    }

    /**
     * @param TPastry $idpastry
     */
    public function setIdpastry($idpastry)
    {
        $this->idpastry = $idpastry;
    }

    /**
     * @return string
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * @param string $image
     */
    public function setImage($image)
    {
        $this->image = $image;
    }



}

