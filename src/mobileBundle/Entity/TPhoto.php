<?php

namespace mobileBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * TPhoto
 *
 * @ORM\Table(name="t_photo", indexes={@ORM\Index(name="FK_n544imhhnkq2pjt588vf00s88", columns={"idPastry"}), @ORM\Index(name="FK_tm1hmm7uliijejqx69oyn1uhq", columns={"idProduit"}), @ORM\Index(name="FK_g07o1exes1y2u28034sfabnyy", columns={"idUser"})})
 * @ORM\Entity
 */
class TPhoto
{
    /**
     * @var string
     *
     * @ORM\Column(name="Urlphotos", type="string", length=255, nullable=true)
     */
    private $urlphotos;

    /**
     * @var string
     *
     * @ORM\Column(name="legende", type="string", length=255, nullable=true)
     */
    private $legende;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \mobileBundle\Entity\TUser
     *
     * @ORM\ManyToOne(targetEntity="mobileBundle\Entity\TUser")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="idUser", referencedColumnName="id",onDelete="SET NULL")
     * })
     */
    private $iduser;

    /**
     * @var \mobileBundle\Entity\TPastry
     *
     * @ORM\ManyToOne(targetEntity="mobileBundle\Entity\TPastry")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="idPastry", referencedColumnName="id",onDelete="SET NULL")
     * })
     */
    private $idpastry;

    /**
     * @var \mobileBundle\Entity\TProduit
     *
     * @ORM\ManyToOne(targetEntity="mobileBundle\Entity\TProduit")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="idProduit", referencedColumnName="id",onDelete="SET NULL")
     * })
     */
    private $idproduit;

    /**
     * @return string
     */
    public function getUrlphotos()
    {
        return $this->urlphotos;
    }

    /**
     * @param string $urlphotos
     */
    public function setUrlphotos($urlphotos)
    {
        $this->urlphotos = $urlphotos;
    }

    /**
     * @return string
     */
    public function getLegende()
    {
        return $this->legende;
    }

    /**
     * @param string $legende
     */
    public function setLegende($legende)
    {
        $this->legende = $legende;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return TUser
     */
    public function getIduser()
    {
        return $this->iduser;
    }

    /**
     * @param TUser $iduser
     */
    public function setIduser($iduser)
    {
        $this->iduser = $iduser;
    }

    /**
     * @return TPastry
     */
    public function getIdpastry()
    {
        return $this->idpastry;
    }

    /**
     * @param TPastry $idpastry
     */
    public function setIdpastry($idpastry)
    {
        $this->idpastry = $idpastry;
    }

    /**
     * @return TProduit
     */
    public function getIdproduit()
    {
        return $this->idproduit;
    }

    /**
     * @param TProduit $idproduit
     */
    public function setIdproduit($idproduit)
    {
        $this->idproduit = $idproduit;
    }


}

