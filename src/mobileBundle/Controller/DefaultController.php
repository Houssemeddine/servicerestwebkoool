<?php

namespace mobileBundle\Controller;

use mobileBundle\Entity\TSubscribe;
use mobileBundle\Entity\TUser;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Acl\Exception\Exception;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;

class DefaultController extends Controller
{
    public function  loginAction(Request $request){

        $em =$this->getDoctrine()->getManager();
        $password=$request->get("password");
        $email=$request->get("email");
        $user = new TUser();
        $user->setId(0);

        $user=$em->getRepository("mobileBundle\\Entity\\TUser")->findOneBy(array('email'=>$email,'password'=>$password));

        if ($user->getId() !=0){
            $serializer = new Serializer([new ObjectNormalizer()]);
            $formatted = $serializer->normalize($user);
            return new JsonResponse($formatted);
        }else{
            return "faild";
        }
    }



    public function ImagePastryAction(Request $request){

               $nom=$request->get("legende");
               $image=$request->get("image");


              if (file_put_contents("C:/wamp64/www/ServiceMobile/mobileService/web/upload/$nom.jpg"
                  ,base64_decode($image))) {

                  $user= new TUser();
                  $user->setPassword("sucess");
                  $serializer = new Serializer([new ObjectNormalizer()]);
                  $formatted = $serializer->normalize($user);
                  return new JsonResponse($formatted);
              }

        $user= new TUser();
        $user->setPassword("sucess");
        $serializer = new Serializer([new ObjectNormalizer()]);
        $formatted = $serializer->normalize($user);
        return new JsonResponse($formatted);




        }



public  function instancierPastryToUserAction(Request $request){

    $em =$this->getDoctrine()->getManager();

        $idUser=$request->get("user");
        $idPastry=$request->get("pastry");

        $User=$em->getRepository("mobileBundle\\Entity\\TUser")->find($idUser);
        $pastry=$em->getRepository("mobileBundle\\Entity\\TPastry")->find($idPastry);

    $User->setIdpastry($pastry);
    $em->persist($User);
    $em->flush();
    $serializer = new Serializer([new ObjectNormalizer()]);
    $formatted = $serializer->normalize($User);
    return new JsonResponse($formatted);

}





    public  function getProduitFromPastryAction(Request $request){
        $em =$this->getDoctrine()->getManager();
        $idPastry=$request->get("pastry");
       $produits=$em->getRepository("mobileBundle\\Entity\\TProduit")->findBy(array("idpastry"=>$idPastry));

        $serializer = new Serializer([new ObjectNormalizer()]);
        $formatted = $serializer->normalize($produits);
        return new JsonResponse($formatted);

    }

    public function verifierAbonnerPastryAction(Request $request){
        $em =$this->getDoctrine()->getManager();
        $idPastry=$request->get("pastry");
        $idUser=$request->get("user");

        $subscribe=$em->getRepository('mobileBundle\Entity\TSubscribe')->findOneBy(array('idpastry'=>$idPastry,'iduser'=>$idUser));
if (!$subscribe){

    return new JsonResponse(array("subscribe"=>false));
}else{
    return new JsonResponse(array("subscribe"=>true));

}
    }
    public function verifierAbonnerProductAction(Request $request){
        $em =$this->getDoctrine()->getManager();
        $idPastry=$request->get("product");
        $idUser=$request->get("user");

        $subscribe=$em->getRepository('mobileBundle\Entity\TSubscribe')->findOneBy(array('idproduit'=>$idPastry,'iduser'=>$idUser));
if (!$subscribe){

    return new JsonResponse(array("subscribe"=>false));
}else{
    return new JsonResponse(array("subscribe"=>true));

}
    }
    public function desabonnerPastryAction(Request $request){
    $em =$this->getDoctrine()->getManager();
    $idPastry=$request->get("idpastry");
    $idUser=$request->get("iduser");
    $subscribes=$em->getRepository('mobileBundle\Entity\TSubscribe')->findBy(array('idpastry'=>$idPastry,'iduser'=>$idUser));
    foreach ($subscribes as$subscribe ){
        $em->remove($subscribe);
        $em->flush();
    }
    return new JsonResponse(array("desabonner"=>true));
}
    public function desabonnerProductAction(Request $request){
    $em =$this->getDoctrine()->getManager();
        $idPastry=$request->get("idproduit");
    $idUser=$request->get("iduser");
    $subscribes=$em->getRepository('mobileBundle\Entity\TSubscribe')->findBy(array('idproduit'=>$idPastry,'iduser'=>$idUser));
    foreach ($subscribes as$subscribe ){
        $em->remove($subscribe);
        $em->flush();
    }
    return new JsonResponse(array("desabonner"=>true));
}

    public function abonnerPastryAction(Request $request){
        $em =$this->getDoctrine()->getManager();
        $idPastry=$request->get("idpastry");
        $idUser=$request->get("iduser");
        $subscribes=$em->getRepository('mobileBundle\Entity\TSubscribe')->findBy(array('idpastry'=>$idPastry,'iduser'=>$idUser));
        foreach ($subscribes as$subscribe ){
            return new JsonResponse(array("abonner"=>false));

        }
        $subscribe = new TSubscribe();
$subscribe->setIdpastry($em->getRepository('mobileBundle\Entity\TPastry')->find($idPastry));
$subscribe->setIduser($em->getRepository('mobileBundle\Entity\TUser')->find($idUser));
$em->persist($subscribe);
$em->flush();

        return new JsonResponse(array("abonner"=>true));
    }
    public function abonnerProductAction(Request $request){
        $em =$this->getDoctrine()->getManager();
        $idPastry=$request->get("idproduit");
        $idUser=$request->get("iduser");
        $subscribes=$em->getRepository('mobileBundle\Entity\TSubscribe')->findBy(array('idproduit'=>$idPastry,'iduser'=>$idUser));
        foreach ($subscribes as$subscribe ){
            return new JsonResponse(array("abonner"=>false));

        }
        $subscribe = new TSubscribe();
        $subscribe->setIdproduit($em->getRepository('mobileBundle\Entity\TProduit')->find($idPastry));
        $subscribe->setIduser($em->getRepository('mobileBundle\Entity\TUser')->find($idUser));
        $em->persist($subscribe);
        $em->flush();

        return new JsonResponse(array("abonner"=>true));
    }

    public function sumAbonnerPastryAction(Request $request){

        $em =$this->getDoctrine()->getManager();
        $idPastry=$request->get("idpastry");
        $subscribes=$em->getRepository('mobileBundle\Entity\TSubscribe')->findBy(array('idpastry'=>$idPastry));

        $i=0;
        foreach ($subscribes as $subscribe ){

            $i+=1;
        }

        return new JsonResponse(array("somme"=>$i));
    }

    public function sumAbonnerProductAction(Request $request){

        $em =$this->getDoctrine()->getManager();
        $idPastry=$request->get("idproduit");
        $subscribes=$em->getRepository('mobileBundle\Entity\TSubscribe')->findBy(array('idproduit'=>$idPastry));

        $i=0;
        foreach ($subscribes as $subscribe ){

            $i+=1;
        }

        return new JsonResponse(array("somme"=>$i));
    }


    public function indexAction()
    {
        return $this->render('mobileBundle:Default:index.html.twig');
    }

    public function getAvisPastryAction(Request $request){

        $em =$this->getDoctrine()->getManager();
        $idPastry=$request->get("idpastry");
        $avis=$em->getRepository("mobileBundle\\Entity\\TNote")->findBy(array("idpastry"=>$idPastry));
        $serializer = new Serializer([new ObjectNormalizer()]);
        $formatted = $serializer->normalize($avis);
        return new JsonResponse($formatted);

    }
    public function getAvisProductAction(Request $request){

        $em =$this->getDoctrine()->getManager();
        $idPastry=$request->get("idproduit");
        $avis=$em->getRepository("mobileBundle\\Entity\\TNote")->findBy(array("idproduit"=>$idPastry));
        $serializer = new Serializer([new ObjectNormalizer()]);
        $formatted = $serializer->normalize($avis);
        return new JsonResponse($formatted);

    }
    public function getSumAvisPastryAction(Request $request){


        $em =$this->getDoctrine()->getManager();
        $idPastry=$request->get("idpastry");
        $subscribes=$em->getRepository('mobileBundle\Entity\TNote')->findBy(array('idpastry'=>$idPastry));

        $i=0;
        foreach ($subscribes as $subscribe ){

            $i+=1;
        }

        return new JsonResponse(array("somme"=>$i));
    }public function getsumAvisProductAction(Request $request){


        $em =$this->getDoctrine()->getManager();
        $idPastry=$request->get("idproduit");
        $subscribes=$em->getRepository('mobileBundle\Entity\TNote')->findBy(array('idproduit'=>$idPastry));

        $i=0;
        foreach ($subscribes as $subscribe ){

            $i+=1;
        }

        return new JsonResponse(array("somme"=>$i));
    }
    public function getpastrySubscribeAction(Request $request){


        $em =$this->getDoctrine()->getManager();
        $iduser=$request->get("iduser");
        $subscribes=$em->getRepository('mobileBundle\Entity\TSubscribe')
            ->findBy(array("iduser"=>$iduser,"idproduit"=>null));

        $serializer = new Serializer([new ObjectNormalizer()]);
        $formatted = $serializer->normalize($subscribes);
        return new JsonResponse($formatted);
    }
public function getnewPastryAction(){
    $em =$this->getDoctrine()->getManager();
    $pastry=$em->getRepository('mobileBundle\Entity\TPastry')->getNew();

    $serializer = new Serializer([new ObjectNormalizer()]);
    $formatted = $serializer->normalize($pastry);
    return new JsonResponse($formatted);

}public function getPopulairePastryAction(){
    $em =$this->getDoctrine()->getManager();
    $pastry=$em->getRepository('mobileBundle\Entity\TPastry')->getPopulaire();

    $serializer = new Serializer([new ObjectNormalizer()]);
    $formatted = $serializer->normalize($pastry);
    return new JsonResponse($formatted);

}
public function getnewProductAction(){
    $em =$this->getDoctrine()->getManager();
    $pastry=$em->getRepository('mobileBundle\Entity\TProduit')->getNew();

    $serializer = new Serializer([new ObjectNormalizer()]);
    $formatted = $serializer->normalize($pastry);
    return new JsonResponse($formatted);

}public function getPopulaireProductAction(){
    $em =$this->getDoctrine()->getManager();
    $pastry=$em->getRepository('mobileBundle\Entity\TProduit')->getPopulaire();

    $serializer = new Serializer([new ObjectNormalizer()]);
    $formatted = $serializer->normalize($pastry);
    return new JsonResponse($formatted);

}

public function getProximityPastryAction(Request $request){
    $em =$this->getDoctrine()->getManager();
    $latitude=$request->get("latitude");
    $longitude=$request->get("longitude");
    $pastry=$em->getRepository('mobileBundle\Entity\TPastry')->getProximity($latitude,$longitude);
    $serializer = new Serializer([new ObjectNormalizer()]);
    $formatted = $serializer->normalize($pastry);
    return new JsonResponse($formatted);

}



}
