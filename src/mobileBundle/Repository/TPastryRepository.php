<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 30/12/2018
 * Time: 12:34
 */

namespace mobileBundle\Repository;


class TPastryRepository extends \Doctrine\ORM\EntityRepository
{


    public function getNew(){

        return $this->getEntityManager()->createQuery('
    SELECT c
    FROM mobileBundle:TPastry c
    ORDER BY c.id DESC '
        )->setMaxResults(5)->getResult();

    }
    public function getPopulaire(){
        return $this->getEntityManager()->createQuery('
SELECT p AS pastry,count(s.id) As nbr FROM mobileBundle:TPastry  p
 JOIN mobileBundle:TSubscribe s WITH p.id = s.idpastry 
 group by p.id 
  ORDER BY nbr DESC 
')->setMaxResults(5)->getResult();
    }

  public function getProximity($latitude,$longitude){
      $classLoader = new \Doctrine\Common\ClassLoader('DoctrineExtensions', '/path/to/extensions');
      $classLoader->register();
        return $this->getEntityManager()->createQuery('SELECT pp AS pastry,(((acos(sin((:jpsLatitude*22/7/180)) * sin((pp.jpsLatitude*22/7/180))+cos((:jpsLatitude*22/7/180)) * cos((pp.jpsLatitude*22/7/180)) * cos(((:jpsLongitude - pp.jpsLongitude)*22/7/180))))*180/22/7)*60*1.1515*1.609344) as distance FROM mobileBundle:TPastry pp ORDER BY distance ASC')->setParameter("jpsLatitude",$latitude)->setParameter("jpsLongitude",$longitude)->setMaxResults(6)->getResult();
    }


}