<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 30/12/2018
 * Time: 22:39
 */

namespace mobileBundle\Repository;


class TProduitRepository extends \Doctrine\ORM\EntityRepository
{
    public function getNew(){

        return $this->getEntityManager()->createQuery('
    SELECT c
    FROM mobileBundle:TProduit c
    ORDER BY c.id DESC '
        )->setMaxResults(5)->getResult();

    }
    public function getPopulaire(){
        return $this->getEntityManager()->createQuery('
SELECT p AS produit,count(s.id) As nbr FROM mobileBundle:TProduit  p
 JOIN mobileBundle:TSubscribe s WITH p.id = s.idproduit 
 group by p.id 
  ORDER BY nbr DESC 
')->setMaxResults(8)->getResult();
    }

}